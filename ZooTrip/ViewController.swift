//
//  ViewController.swift
//  ZooTrip
//
//  Created by Elliott Diaz on 5/5/16.
//  Copyright © 2016 Elliott Diaz. All rights reserved.
//

import UIKit

class ViewController: UIViewController
{

    @IBOutlet weak var pictureImageView: UIImageView!
    @IBOutlet weak var nameLabel       : UILabel!
    @IBOutlet weak var speciesLabel    : UILabel!
    @IBOutlet weak var funFactLabel    : UILabel!
    
    var animals: [Animal]  = []
    var currentAnimalIndex = 0
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        let myElephant = Factory.createElephant()
        let myTiger    = Factory.createTiger   ()
        let myLion     = Factory.createLion    ()
        let myLionCub  = Factory.createLionCub ()
        
        
        animals = [ myElephant, myTiger, myLion, myLionCub ]
        updateView()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func nextBarButtonItemPressed(sender: UIButton)
    {
        calculateIndex()
        updateView()    
    }
    
    func updateView()
    {
        let currentAnimal = animals[currentAnimalIndex]
        pictureImageView.alpha = 0.0
        
        UIView.animateWithDuration(1.0)
        {
            self.pictureImageView.alpha = 1.0
            self.nameLabel       .text  = currentAnimal.personalInformation.name
            self.speciesLabel    .text  = currentAnimal.personalInformation.species
            self.pictureImageView.image = currentAnimal.personalInformation.image
            self.funFactLabel    .text  = currentAnimal.randomFact()
        }
    }
    
    func calculateIndex()
    {
        if currentAnimalIndex == animals.count - 1
        {
            currentAnimalIndex = 0
        }
        else
        {
            currentAnimalIndex = currentAnimalIndex + 1
        }
    }
}

