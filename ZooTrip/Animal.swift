//
//  Animal.swift
//  ZooTrip
//
//  Created by Elliott Diaz on 5/6/16.
//  Copyright © 2016 Elliott Diaz. All rights reserved.
//

import Foundation
import UIKit

class Animal
{
    var bodyStatistics      = BodyStatistics     ()
    var personalInformation = PersonalInformation()
    
    func speak()
    {
        print("Animal: Can't Speak Yet!")
    }
    func randomFact() -> String
    {
        return "No Available Random Fact At This Time!"
    }
    func returnImage( animalName : String ) -> UIImage?
    {
        let animalNumber = Int(arc4random_uniform(UInt32(2))+1)
        let imageName    = animalName + animalNumber.description + ".jpeg"
        
        return UIImage( named: imageName )
    }
}
