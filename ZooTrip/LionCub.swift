//
//  LionCub.swift
//  ZooTrip
//
//  Created by Elliott Diaz on 5/6/16.
//  Copyright © 2016 Elliott Diaz. All rights reserved.
//

import Foundation

class LionCub : Lion
{
    override func speak()
    {
        super.speak()
        print("Lion Cub: Baby Roar ")
    }
    override func randomFact() -> String
    {
        return "Lion cubs start walking at 10 to 15 years old!"
    }
}