//
//  Penguin.swift
//  ZooTrip
//
//  Created by Elliott Diaz on 5/6/16.
//  Copyright © 2016 Elliott Diaz. All rights reserved.
//

import Foundation
import UIKit

struct Penguin
{
    var age     : Int
    var name    : String
    var species : String
    var image   : UIImage
    
    func speak()
    {
        print("Penguin: Trumpet Trumpet")
    }
    
    func trumpetANumberOfTime ( numberOfTimes: Int )
    {
        for _ in 1...numberOfTimes
        {
            speak()
        }
    }
    
    func speakANumberOfTimes( numberOfTimes : Int, isLoud: Bool)
    {
        for _ in 0..<numberOfTimes
        {
            if isLoud
            {
                speak()
            }
            else
            {
                print("Penguin: bray bray")
            }
        }
    }
    
    func ageInPenguinYearsFromHumanYears() -> Int
    {
        return age * 3
    }
    
    func randomFact() -> String
    { 
        let randomNumber = Int(arc4random_uniform(3))
        let randomFactArray = ["All 17 species of penguins are naturally found exclusively in the Southern Hemispere.", "Penguins striking coloring is a matter of camouflage", "Unlike most birds, which lose and replace a few feathers at a time, penguins molt all at once!"]
        return randomFactArray[randomNumber]
    }
}