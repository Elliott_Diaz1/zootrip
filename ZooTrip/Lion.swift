//
//  Lion.swift
//  ZooTrip
//
//  Created by Elliott Diaz on 5/6/16.
//  Copyright © 2016 Elliott Diaz. All rights reserved.
//

import Foundation

class Lion : Animal
{
    override func speak()
    {
        print("Lion: Mighty Rawr")
    }
    
    override func randomFact() -> String
    {
        return "Lions are the second largest big cat species in the world (behind tigers)."
    }
    
}