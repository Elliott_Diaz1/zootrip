//
//  Factory.swift
//  ZooTrip
//
//  Created by Elliott Diaz on 5/6/16.
//  Copyright © 2016 Elliott Diaz. All rights reserved.
//

import Foundation

class Factory
{
    class func createTiger() -> Tiger
    {
        let tiger = Tiger()
        tiger.personalInformation.image   = tiger.returnImage("Tiger")
        tiger.personalInformation.species = "Bengal"
        tiger.personalInformation.name    = "Sheir Khan"
        tiger.bodyStatistics     .height  = 200
        return tiger
    }
    class func createElephant() -> Elephant
    {
        let elephant = Elephant()
        elephant.personalInformation.image   = elephant.returnImage("Elephant")
        elephant.personalInformation.species = "African"
        elephant.personalInformation.name    = "Dumbo"
        elephant.bodyStatistics     .height  = 750
        return elephant
    }
    class func createLion() -> Lion
    {
        let lion = Lion()
        lion.personalInformation.image   = lion.returnImage("Lion")
        lion.personalInformation.species = "Barbary"
        lion.personalInformation.name    = "Mufasa"
        lion.bodyStatistics     .height  = 150
        return lion
    }
    class func createLionCub() -> LionCub
    {
        let lionCub = LionCub()
        lionCub.personalInformation.image   = lionCub.returnImage("LionCub")
        lionCub.personalInformation.species = "Cave Lion"
        lionCub.personalInformation.name    = "Simba"
        lionCub.bodyStatistics     .height  = 35
        return lionCub
    }
}