//
//  Elephant.swift
//  ZooTrip
//
//  Created by Elliott Diaz on 5/6/16.
//  Copyright © 2016 Elliott Diaz. All rights reserved.
//

import Foundation

class Elephant : Animal
{
    override func speak() {
        print("Elephant: Trump")
    }
    
    override func randomFact() -> String
    {
        return "What is the only mammal that can't jump? The Elephant!"
    }
}