//
//  Tiger.swift
//  ZooTrip
//
//  Created by Elliott Diaz on 5/6/16.
//  Copyright © 2016 Elliott Diaz. All rights reserved.
//

import Foundation

class Tiger : Animal
{
    override func speak()
    {
        super.speak()
        print("Tiger: Chuff ")
    }
    
    override func randomFact() -> String
    {
        return "There are more tigers held privately as pets than there are in the wild."
    }
}