//
//  PersonalInFormation.swift
//  ZooTrip
//
//  Created by Elliott Diaz on 5/6/16.
//  Copyright © 2016 Elliott Diaz. All rights reserved.
//

import Foundation
import UIKit

struct PersonalInformation
{
    var name    = ""
    var species = ""
    var image   = UIImage(named: "")
}